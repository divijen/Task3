package rewards.simulator;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import rewards.simulator.data.SimulatorDao;
import rewards.simulator.pojo.Contract;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class MainSimulator {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String args[]) {
        boolean exit = false;

        while (!exit) {
            System.out.println("\n\nPlease select the number from the sequence:\n1. REGISTER partnerId\n2. REGISTER partnerId parentPartnerId\n3. LOAD filename \n4. LEVEL partnerId year quarter\n5. REWARDS partnerId year quarter\n6. ALL_REWARDS partnerId\n7. Exit the process\n\n");
            String choice = scanner.nextLine();
            try {
                switch (choice) {
                    case "1":
                        register(false);
                        break;
                    case "2":
                        register(true);
                        break;
                    case "3":
                        loadSheet();
                        break;
                    case "4":
                        level();
                        break;
                    case "5":
                        rewards();
                        break;
                    case "6":
                        allRewards();
                        break;
                    case "7":
                        exit = true;
                        break;
                    default:
                        System.out.println("Invalid choice. Please enter a valid choice again\n");
                        break;
                }
            } catch (Exception e) {
                System.out.println("There was an unexpected error.");
                continue;
            }
        }
    }

    public static void register(boolean hasParent) throws Exception {
        String partnerId = null;
        String parentPartnerId = null;
        System.out.println("Please enter partnerId: ");
        partnerId = scanner.nextLine();
        if (hasParent) {
            System.out.println("Please enter parent partnerId: ");
            parentPartnerId = scanner.nextLine();
        }

        if (hasParent ? (!partnerId.isEmpty() && !parentPartnerId.isEmpty()) : !partnerId.isEmpty()) {
            if (!SimulatorDao.findPartnerId(partnerId)) {
                SimulatorDao.registerPartner(partnerId, parentPartnerId);

                System.out.println("Partner registered successfully");
            } else {
                System.out.println("Partner registered already. Please enter a new one.");
            }
        } else {
            System.out.println("Please enter valid inputs");
        }
    }

    public static void loadSheet() throws Exception {
        System.out.println("Please enter filename: ");
        String fileName = scanner.nextLine();

        if (!fileName.isEmpty()) {
            try {
                Reader reader = Files.newBufferedReader(Paths.get(fileName));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);

                //go through every row
                for (CSVRecord csvRecord : csvParser) {
                    String partnerId = csvRecord.get(0);
                    String contractId = csvRecord.get(1);
                    String contractType = csvRecord.get(2);
                    String dateStr = csvRecord.get(3);
                    String action = csvRecord.get(4);

                    //This is to make sure the row is not a column header
                    Date date = null;
                    try {
                        date = new SimpleDateFormat("dd.MM.yyyy").parse(dateStr);
                    } catch (ParseException pe) {
                        System.out.println("\nRow number " + (csvRecord.getRecordNumber()) + " was not inserted into the db. Expected date format is dd.mm.yyyy");
                        continue;
                    }
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);


                    if (contractId != null && partnerId != null) {
                        Contract contract = new Contract();
                        contract.setContractId(contractId);
                        contract.setPartnerId(partnerId);
                        contract.setContractType(contractType);

                        switch (action) {
                            case "BEGIN":
                                contract.setBeginDay(day);
                                contract.setBeginMonth(month + 1);
                                contract.setBeginYear(year);
                                break;
                            case "END":
                                contract.setEndDay(day);
                                contract.setEndMonth(month + 1);
                                contract.setEndYear(year);
                                break;
                        }
                        SimulatorDao.addSales(contract);
                        System.out.println("\nRow number " + csvRecord.getRecordNumber() + " inserted/updated successfully");
                    } else {
                        System.out.println("\nRow number " + csvRecord.getRecordNumber() + " inserted/updated successfully");
                    }
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                System.out.println("Input output error");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
            }
        } else

        {
            System.out.println("Please enter a valid inputs");
        }

    }

    public static void level() throws Exception {
        System.out.println("Please enter partnerId : ");
        String partnerId = scanner.nextLine();
        System.out.println("Please enter year: ");
        String year = scanner.nextLine();
        System.out.println("Please enter quarter: ");
        String quarter = scanner.nextLine();

        if (!partnerId.isEmpty() && !year.isEmpty() && !quarter.isEmpty()) {
            int count = SimulatorDao.getContractsCountForPartner(partnerId, year, quarter);
            if (count <= 0) {
                System.out.println("The partner has no contracts for this quarter and year");
                return;
            }
            LevelEnum level = getLevel(count);
            System.out.println("Level for the quarter " + quarter + " of year " + year + " for partnerId " + partnerId + " is: " + level.name());
        } else {
            System.out.println("Please enter a valid inputs");
        }
    }

    private static LevelEnum getLevel(int count) {
        if (count >= LevelEnum.ANT.getLowerLimit() && count < LevelEnum.ANT.getUpperLimit()) {
            return LevelEnum.ANT;
        } else if (count >= LevelEnum.BEE.getLowerLimit() && count < LevelEnum.BEE.getUpperLimit()) {
            return LevelEnum.BEE;
        } else if (count <= LevelEnum.CAT.getLowerLimit() && count < LevelEnum.CAT.getUpperLimit()) {
            return LevelEnum.CAT;
        } else if (count <= LevelEnum.DOG.getLowerLimit() && count < LevelEnum.DOG.getUpperLimit()) {
            return LevelEnum.DOG;
        } else if (count >= LevelEnum.ELEPHANT.getLowerLimit()) {
            return LevelEnum.ELEPHANT;
        }
        return null;
    }

    public static void rewards() throws Exception {
        System.out.println("Please enter partnerId : ");
        String partnerId = scanner.nextLine();
        System.out.println("Please enter year: ");
        String year = scanner.nextLine();
        System.out.println("Please enter quarter: ");
        String quarter = scanner.nextLine();

        if (!partnerId.isEmpty() && !year.isEmpty() && !quarter.isEmpty()) {
            float rewards = calculateRewards(partnerId, year, quarter);
            System.out.println("The total amount of rewards partner id " + partnerId + " has gained in the year " + year + " for the quarter " + quarter + " is \u20ac" + rewards);
        } else {
            System.out.println("Please enter a valid inputs");
        }
    }

    public static void allRewards() throws Exception {
        System.out.println("Please enter partnerId : ");
        String partnerId = scanner.nextLine();

        if (!partnerId.isEmpty()) {
            int[] years = SimulatorDao.getYear(true);
            int earliestYear = years[0];
            int latestYear = years[1];

            for (int year = earliestYear; year <= latestYear; year++) {
                for (int quarter = 1; quarter <= 4; quarter++) {
                    float rewards = calculateRewards(partnerId, Integer.toString(year), Integer.toString(quarter));
                    System.out.println(year + " " + quarter + " \u20ac" + rewards);
                }
            }
        } else {
            System.out.println("Please enter a valid inputs");
        }
    }

    private static float calculateRewards(String partnerId, String year, String quarter) throws Exception {
        int contractCount = SimulatorDao.getContractsCountForPartner(partnerId, year, quarter);

        LevelEnum parentLevel = getLevel(contractCount);
        int contractAmount = contractCount * parentLevel.getReward();

        int rabbitContracts = SimulatorDao.getRabbitContractsForPartner(partnerId, year, quarter);
        int rabbitBonus = rabbitContracts * 50;

        List<String> childPartnerIds = SimulatorDao.getChildPartnerIds(partnerId);
        int totalChildrenDifferenceAmt = 0;

        for (String childPartnerId : childPartnerIds) {
            int childContractsCount = SimulatorDao.getContractsCountForPartner(childPartnerId, year, quarter);
            LevelEnum childLevel = getLevel(childContractsCount);
            int difference = parentLevel.getReward() - childLevel.getReward();
            if (difference > 0)
                totalChildrenDifferenceAmt = totalChildrenDifferenceAmt + (childContractsCount * difference);
        }

        return contractAmount + rabbitBonus + totalChildrenDifferenceAmt;
    }
}
