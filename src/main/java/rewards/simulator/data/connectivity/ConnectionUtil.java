package rewards.simulator.data.connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public final class ConnectionUtil {
    private static final String url = "jdbc:sqlite:task3.db";

    private ConnectionUtil(){

    }

    public static Connection getConnection() throws SQLException {
        Connection con = DriverManager.getConnection(url);

        PreparedStatement preparedStatement = con.prepareStatement("PRAGMA foreign_keys = ON;");
        preparedStatement.execute();
        return con;
    }
}
