package rewards.simulator.data;

import org.sqlite.SQLiteException;
import rewards.simulator.data.connectivity.ConnectionUtil;
import rewards.simulator.pojo.Contract;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SimulatorDao {

    /**
     * purpose : insert/ update contract details to sales table
     *
     * @param contract
     * @throws Exception
     */
    public static void addSales(Contract contract) throws Exception {

        PreparedStatement preparedStatementUpdate = null;
        Connection con = null;
        ResultSet resultSet = null;
        String query = null;

        try {
            con = ConnectionUtil.getConnection();
            //the contract id already exists
            if (findContractId(contract.getContractId())) {
                if (contract.getBeginDay() > 0) {
                    query = "UPDATE sale SET contract_start_day=?,contract_start_month=?,contract_start_year=? where contract_id = ?";
                    preparedStatementUpdate = con.prepareStatement(query);
                    preparedStatementUpdate.setInt(1, contract.getBeginDay());
                    preparedStatementUpdate.setInt(2, contract.getBeginMonth());
                    preparedStatementUpdate.setInt(3, contract.getBeginYear());
                    preparedStatementUpdate.setString(4, contract.getContractId());
                    preparedStatementUpdate.executeUpdate();
                }
                if (contract.getEndDay() > 0) {
                    query = "UPDATE sale SET contract_end_day=?,contract_end_month=?,contract_end_year=? where contract_id = ?";
                    preparedStatementUpdate = con.prepareStatement(query);
                    preparedStatementUpdate.setInt(1, contract.getEndDay());
                    preparedStatementUpdate.setInt(2, contract.getEndMonth());
                    preparedStatementUpdate.setInt(3, contract.getEndYear());
                    preparedStatementUpdate.setString(4, contract.getContractId());
                    preparedStatementUpdate.executeUpdate();
                }
            } else {
                //New entry
                query = "INSERT INTO sale(contract_id,partner_id,contract_type,contract_start_day,contract_start_month," +
                        "contract_start_year,contract_end_day,contract_end_month,contract_end_year) VALUES(?,?,?,?,?,?,?,?,?)";

                preparedStatementUpdate = con.prepareStatement(query);
                preparedStatementUpdate.setString(1, contract.getContractId());
                preparedStatementUpdate.setString(2, contract.getPartnerId());
                preparedStatementUpdate.setString(3, contract.getContractType());

                preparedStatementUpdate.setInt(4, contract.getBeginDay());
                preparedStatementUpdate.setInt(5, contract.getBeginMonth());
                preparedStatementUpdate.setInt(6, contract.getBeginYear());

                preparedStatementUpdate.setInt(7, contract.getEndDay());
                preparedStatementUpdate.setInt(8, contract.getEndMonth());
                preparedStatementUpdate.setInt(9, contract.getEndYear());
                preparedStatementUpdate.executeUpdate();
            }


        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
            System.out.println("Error while updating/inserting to the database");
            throw sqle;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error while updating/inserting to the database");
            throw sqle;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatementUpdate != null) {
                    preparedStatementUpdate.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    public static boolean findContractId(String contractId) throws Exception {
        Statement statementSelect = null;
        Connection con = null;
        ResultSet resultSet = null;
        try {
            con = ConnectionUtil.getConnection();
            String query = "select count(*) from sale where contract_id = " + contractId;
            statementSelect = con.createStatement();
            resultSet = statementSelect.executeQuery(query);
            return resultSet.getInt(1) > 0;
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
            System.out.println("Error while updating/inserting to the database");
            throw sqle;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error while updating/inserting to the database");
            throw sqle;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statementSelect != null) {
                    statementSelect.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    public static boolean findPartnerId(String partnerId) throws Exception {
        Statement statementSelect = null;
        Connection con = null;
        ResultSet resultSet = null;
        try {
            con = ConnectionUtil.getConnection();
            String query = "select count(*) from partner where partner_id = " + partnerId;
            statementSelect = con.createStatement();
            resultSet = statementSelect.executeQuery(query);
            return resultSet.getInt(1) > 0;
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
            System.out.println("Error while updating/inserting to the database");
            throw sqle;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error while updating/inserting to the database");
            throw sqle;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statementSelect != null) {
                    statementSelect.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    /**
     * purpose : add a partner to database
     *
     * @param partnerId
     * @param parentPartnerId
     * @throws Exception
     */
    public static void registerPartner(String partnerId, String parentPartnerId) throws Exception {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String query = "INSERT INTO partner(partner_id,parent_partner_id,registration_date,is_active) VALUES(?,?,?,?)";
        try {
            connection = ConnectionUtil.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, partnerId);
            preparedStatement.setString(2, parentPartnerId);
            preparedStatement.setString(3, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            preparedStatement.setInt(4, 1);

            preparedStatement.execute();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error during database insert");
            throw sqle;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The unexpected has happened.");
            throw e;
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    /**
     * purpose : Get sum of all the contracts for given partner, year, quarter
     *
     * @param partnerId
     * @param yearStr
     * @param quarterStr
     * @return number of contracts for given partner
     * @throws Exception
     */
    public static int getContractsCountForPartner(String partnerId, String yearStr, String quarterStr) throws Exception {
        Statement statementSelect = null;
        Connection con = null;
        ResultSet resultSet = null;
        int quarter = Integer.parseInt(quarterStr);
        int year = Integer.parseInt(yearStr);

        try {
            con = ConnectionUtil.getConnection();
            String query = "select count(*) from sale where partner_id = " + partnerId + " and contract_start_month IN (" + (3 * quarter - 2) + "," + (3 * quarter - 1) + "," + (3 * quarter) + ") and contract_start_year BETWEEN " + (year - 8) + " AND " + (year);

            statementSelect = con.createStatement();
            resultSet = statementSelect.executeQuery(query);
            int parentContracts = resultSet.getInt(1);
            List<String> childPartnerIds = getChildPartnerIds(partnerId);
            int childContracts = 0;
            //make recursive call to fetch child partner ids
            if (childPartnerIds != null && childPartnerIds.size() > 0) {
                for (String childPartnerId : childPartnerIds) {
                    childContracts = childContracts + getContractsCountForPartner(childPartnerId, yearStr, quarterStr);
                }
                return childContracts + parentContracts;
            } else {
                return parentContracts;
            }

        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error during database select");
            throw sqle;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The unexpected has happened.");
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statementSelect != null) {
                    statementSelect.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    /**
     * purpose : get all the rabbit contracts for given partner in given year and quarter
     *
     * @param partnerId
     * @param yearStr
     * @param quarterStr
     * @return rabbit contracts for given partner in given year and quarter
     * @throws Exception
     */
    public static int getRabbitContractsForPartner(String partnerId, String yearStr, String quarterStr) throws Exception {
        Statement statementSelect = null;
        Connection con = null;
        ResultSet resultSet = null;
        int quarter = Integer.parseInt(quarterStr);
        int year = Integer.parseInt(yearStr);
        try {
            con = ConnectionUtil.getConnection();
            String query = "select count(*) from sale where partner_id = " + partnerId + " and contract_start_month IN (" + (3 * quarter - 2) + "," + (3 * quarter - 1) + "," + (3 * quarter) + ") and contract_start_year = " + year + " and contract_type = 'Rabbit'";

            statementSelect = con.createStatement();
            resultSet = statementSelect.executeQuery(query);

            return resultSet.getInt(1);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error during database select");
            throw sqle;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The unexpected has happened.");
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statementSelect != null) {
                    statementSelect.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    /**
     * purpose : get all the child partner ids of a given partner id
     *
     * @param partnerId
     * @return
     * @throws Exception
     */
    public static List<String> getChildPartnerIds(String partnerId) throws Exception {
        Statement statementSelect = null;
        Connection con = null;
        ResultSet resultSet = null;
        List<String> childPartnerIds = null;
        try {
            con = ConnectionUtil.getConnection();
            String query = "select partner_id from partner where parent_partner_id = " + partnerId;

            statementSelect = con.createStatement();
            resultSet = statementSelect.executeQuery(query);

            childPartnerIds = new ArrayList<>();
            while (resultSet.next()) {
                childPartnerIds.add(resultSet.getString("partner_id"));
            }

            return childPartnerIds;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error during database select");
            throw sqle;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The unexpected has happened.");
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statementSelect != null) {
                    statementSelect.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }

    /**
     * Purpose : Get the earliest and latest from all the sales
     *
     * @return [earliest year,latest year]
     * @throws Exception
     */
    public static int[] getYear(boolean earliest) throws Exception {
        Statement statementSelect = null;
        Connection con = null;
        ResultSet resultSet = null;
        int[] years = new int[2];

        try {
            con = ConnectionUtil.getConnection();
            String query = null;
            if (earliest)
                query = "select min(contract_start_year),max(contract_start_year) from sale";

            statementSelect = con.createStatement();
            resultSet = statementSelect.executeQuery(query);
            years[0] = resultSet.getInt(1);
            years[1] = resultSet.getInt(2);

            return years;

        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error during database select");
            throw sqle;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The unexpected has happened.");
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statementSelect != null) {
                    statementSelect.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException sqle) {
                sqle.printStackTrace();
                System.out.println("Error during db connection cleanup");
                throw sqle;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("The unexpected has happened.");
                throw e;
            }
        }
    }
}
