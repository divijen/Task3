package rewards.simulator;

public enum LevelEnum {
    ANT(5,1,1,10),BEE(7,2,10,50),CAT(9,3,50,200),DOG(12,4,200,1000),ELEPHANT(19,5,1000,0);

    private int reward;
    private int id;
    private int lowerLimit;
    private int upperLimit;

    LevelEnum(int reward,int id,int lowerLimit, int upperLimit){
        this.reward = reward;
        this.id = id;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(int lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(int upperLimit) {
        this.upperLimit = upperLimit;
    }
}
