Assumptions:
 The Excel sheet has date in the format dd.mm.yyyy
 The Excel sheet is placed in the location where the jar is being executed
 The db file is placed in the location where the jar is being executed
 sqlite3 is installed on the system

Steps to generate a jar file:
	a. Navigate to the Project root folder and execute the following command
		./gradlew build
	b. Find the jar file "Task3.jar" in the directory build/libs/

Steps to execute the program:
From an executable jar file.
	a. Open terminal where the jar is placed and execute:
		java -jar Task3.jar